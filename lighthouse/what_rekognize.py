

import json
import boto3
import base64
from urllib.request import urlopen


client = boto3.client('rekognition')
event = {'resource': '/categorisation/images', 'path': '/categorisation/images', 'httpMethod': 'POST', 'headers': {'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate, br', 'Content-Type': 'application/json', 'Host': 'afx6ut1rh4.execute-api.eu-west-1.amazonaws.com', 'Postman-Token': 'b0ce201e-2685-4016-b34d-6e19ad2fd494', 'User-Agent': 'PostmanRuntime/7.28.4', 'X-Amzn-Trace-Id': 'Root=1-617dc31b-3989a18f32f12ed11d1a7684', 'X-Forwarded-For': '165.49.27.36', 'X-Forwarded-Port': '443', 'X-Forwarded-Proto': 'https'}, 'multiValueHeaders': {'Accept': ['*/*'], 'Accept-Encoding': ['gzip, deflate, br'], 'Content-Type': ['application/json'], 'Host': ['afx6ut1rh4.execute-api.eu-west-1.amazonaws.com'], 'Postman-Token': ['b0ce201e-2685-4016-b34d-6e19ad2fd494'], 'User-Agent': ['PostmanRuntime/7.28.4'], 'X-Amzn-Trace-Id': ['Root=1-617dc31b-3989a18f32f12ed11d1a7684'], 'X-Forwarded-For': ['165.49.27.36'], 'X-Forwarded-Port': ['443'], 'X-Forwarded-Proto': ['https']}, 'queryStringParameters': None, 'multiValueQueryStringParameters': None, 'pathParameters': None, 'stageVariables': None, 'requestContext': {'resourceId': 'wk2hhe', 'resourcePath': '/categorisation/images', 'httpMethod': 'POST', 'extendedRequestId': 'ICtsXHzqDoEFnpQ=', 'requestTime': '30/Oct/2021:22:11:39 +0000', 'path': '/staging/categorisation/images', 'accountId': '439956857058', 'protocol': 'HTTP/1.1', 'stage': 'staging', 'domainPrefix': 'afx6ut1rh4', 'requestTimeEpoch': 1635631899720, 'requestId': 'c5fbe3c6-8dd8-4888-b157-8cef51b3a044', 'identity': {'cognitoIdentityPoolId': None, 'accountId': None, 'cognitoIdentityId': None, 'caller': None, 'sourceIp': '165.49.27.36', 'principalOrgId': None, 'accessKey': None, 'cognitoAuthenticationType': None, 'cognitoAuthenticationProvider': None, 'userArn': None, 'userAgent': 'PostmanRuntime/7.28.4', 'user': None}, 'domainName': 'afx6ut1rh4.execute-api.eu-west-1.amazonaws.com', 'apiId': 'afx6ut1rh4'}, 'body': '{\n    "image_url": "https://lighthouse-evaluation-images.s3.eu-west-1.amazonaws.com/porn/074a205e2eaf0adb0824d78f38740115.jpg"\n}', 'isBase64Encoded': False}
context = {}

def lambda_handler(event, context):
    print('Start')
    print(event)
    client=boto3.client('rekognition')
    print('Got client')
    if event['httpMethod'] != 'POST':
        print('Not a POST')
        return {
            'statusCode': 405,
            'body': json.dumps({
                'Error' : 'Only POST supported.'
                })
        }
    body = json.loads(event['body'])
    print(body)
    if body['image_url']:
        image_bytes = urlopen(body['image_url']).read()


    response = client.detect_moderation_labels(
        Image={
            'Bytes': image_bytes,
            # 'S3Object': {
            #     'Bucket': 'lighthouse-evaluation-images',
            #     'Name': 'porn/cbbbda8024a2c19557fabcbabaeb3d0b.jpg'
            # }
        },
        MinConfidence=50.0,

    )
    print('Got response')
    print(json.dumps(response, indent=4))
    if response['ModerationLabels']
    for name, age in dictionary.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
        if age == search_age:
            print(name)
    print(json.dumps(response['ModerationLabels'], indent=4))
    return {
        'statusCode': 200,
        'body': json.dumps(response['ModerationLabels'])
    }

lambda_handler(event, context)