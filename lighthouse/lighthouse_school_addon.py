"""
An MITMProxy plugin used by Lighthouse School to filter content

Run as follows: mitmproxy -s lighthouse_school_addon.py
"""
import base64
import cgi
import re

import magic
from mitmproxy import ctx, http
from mitmproxy.http import HTTPFlow


class SchoolProxy:
    def __init__(self):
        self.num = 0
        self.moderation_threshold = 90.0

    def response(self, flow: HTTPFlow):
        """
        The MITMProxy event handler for serving an HTTP(S) response
        """
        maintype, subtype = self.determine_type(flow=flow)
        ctx.log.info("MIME maintype, subtype is {}, {}".format(maintype, subtype))
        if maintype == "image":
            ctx.log.info("Handling image")
            if self.should_image_be_filtered(flow=flow):
                self.serve_blocked_image(flow=flow)
        if maintype == "video":
            ctx.log.info("Handling video")
            if self.should_video_be_filtered(flow=flow):
                self.serve_blocked_image(flow=flow)
        if maintype == "text" and subtype == "html":
            ctx.log.info("Handling HTML for embedded base64 images")
            if self.should_html_be_filtered(flow=flow):
                #self.serve_blocked_image(flow=flow)
                self.replace_embedded_images(flow=flow)


    def determine_type(self, flow: HTTPFlow) -> str:
        """
        Determines the type of content being inspected.

        Returns:
            MIME main type, MIME subtype
        """
        # We look at the self reported content type first, which is quick and
        # computationally cheap check, but might miss some cases
        if "content-type" in flow.response.headers:
            content_type = flow.response.headers["content-type"]
            mimetype, options = cgi.parse_header(content_type)
            maintype, subtype = mimetype.split('/')
            ctx.log.info("Reported MIME: %s" % flow.response.headers["content-type"])
            ctx.log.info("maintype     : %s" % maintype )
            return maintype, subtype

        ctx.log.info("Magic MIME:    %s" % magic.from_buffer(flow.response.get_content(strict=False), mime=True))
        mimetype = magic.from_buffer(flow.response.get_content(strict=False), mime=True)
        maintype, subtype = maintype, subtype = mimetype.split('/')
        return maintype, subtype

    def should_image_be_filtered(self, flow: HTTPFlow) -> bool:
        return True

    def should_video_be_filtered(self, flow: HTTPFlow) -> bool:
        return True

    def should_html_be_filtered(self, flow: HTTPFlow) -> bool:
        page_text = flow.response.text
        if "data:" in page_text:
            return True
        else:
            return False


    def serve_blocked_image(self, flow: HTTPFlow):
        flow.response = http.Response.make(
            200,  # (optional) status code
            blocked_image_data,  # (optional) content
            {"Content-Type": "image/png"}  # (optional) headers
        )

    def replace_embedded_images(self, flow: HTTPFlow):
        replaced = re.sub(b'data:.*,.+', b'data:,'+base64_blocked_image_data, flow.response.content)
        flow.response.content = replaced

addons = [
    SchoolProxy()
]

with open("blocked.png", "rb") as image_file:
    blocked_image_data = image_file.read()
    base64_blocked_image_data = base64.bencode(blocked_image_data)


